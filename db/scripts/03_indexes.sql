alter table book add constraint pk_book_id primary key (id);
alter table book add constraint uc_isbn unique (isbn);
alter table member add constraint pk_member_id primary key (id);
alter table member add constraint uc_insz unique (insz);
alter table loan add constraint pk_loan_id primary key (id);
alter table loan add constraint fk_book_isbn foreign key (isbn) references book (isbn);
alter table loan add constraint fk_member_insz foreign key (memberInsz) references member (insz);