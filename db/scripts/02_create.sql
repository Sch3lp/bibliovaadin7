create table hibernate_sequences (
  sequence_name varchar(255),
  sequence_next_hi_value int
);

create table book (
	id bigint not null,
	isbn varchar(255) not null,
	title varchar(255) not null,
	name varchar(255),
	firstname varchar(255)
);

create table member (
	id bigint not null,
	insz bigint not null,
	name varchar(255) not null,
	firstname varchar(255),
	street varchar(255),
	housenumber bigint,
	postalcode bigint,
	city varchar(255) not null
);

create table loan(
	id bigint not null,
	isbn varchar(255) not null,
	memberInsz bigint not null,
	startDate date not null,
	endDate date,
	fine bigint
);