Installation

Obtain the BiblioVaadin_ENV.rar - no more instructions here just find it;

	1. Unzip to a local directory e.g. eclipse.helios.vaadin;
	2. In directory start eclipse  eclipse.bat (This will set environment variables for maven java eclipse etc) 
	3. Go to Windows|Preferences|Maven

		1. Check Hide Folders of physically nested modules(experimental) 
		2. Set Goals to run when updating project configuration to Compile

	4. Go to Windows|Preferences|Maven|User Settings
		1. Browse to eclipse.helios.vaadin\apache-maven-2.2.1\conf\settings.xml(see below)
		2. Select this file
		3. Press Update Settings - Very important!


Checkout Bibliothouris

	1. In Tortoise check out https://svn.codespaces.com/cegeka/vaadin/Story0 

Import SVN Maven Project into Eclipse

	1. Select File|Import|Maven|Existing Maven Project
	2. Chose eclipse.helios.vaadin\workspace\Story0
	3. In Run Configurations change BiblioVaadin - Full build base directory to ${workspace_loc:/Story0}
	4. In Run Configurations change BiblioVaadin - Full build with Smoketests base directory to ${workspace_loc:/Story0}


Internet Settings
IE setup: Internet options > Security > Internet AND Local intranet AND trusted sites AND Restricted sites enable protected Mode (ON)
     

Test Installation

	1. Run BiblioVaadin - Full build with Smoketests should be no errors
	2. Run Java Application Bibliothouris - Start application

		1. In a browser type http://localhost:8080
		2. Should be a screen with buttons to be pressed and text changes



Maven Settings
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

<localRepository>D:\Bibliothouris\m2repo</localRepository>

    <servers>
        <server>
            <id>nexus</id>
            <username>notaries</username>
            <password>notaries</password>
        </server>

    </servers>
    <mirrors>
        <mirror>
            <id>nexus</id>
            <mirrorOf>*</mirrorOf>
            <url>http://nexus.cegeka.be/nexus/content/groups/public</url>
        </mirror>
    </mirrors>
    <profiles>
        <profile>
            <id>nexus</id>
            <repositories>
                <repository>
                    <id>nexus</id>
                    <url>http://nexus</url>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <id>nexus</id>
                    <url>http://nexus</url>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                </pluginRepository>
            </pluginRepositories>
        </profile>
    </profiles>

    <activeProfiles>
        <activeProfile>nexus</activeProfile>
    </activeProfiles>

</settings>
