package com.cegeka.bibliothouris.domain;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class BookRepository extends AbstractRepository<Book> {

    public Book find(long id) {
        return find(Book.class, id);
    }

    public List<Book> findAllBooks() {
        return getBooksFromQuery("from Book");
    }

    public List<Book> findBooksByISBN(Isbn isbn) {
        if (isbn != null) {
            return getBooksFromQuery("from Book where isbn like '" + isbn.getIsbnValue().replace('*', '%') + "'");
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    private List<Book> getBooksFromQuery(String query) {
        return entityManager.createQuery(query).getResultList();
    }

    public Book findBookBySingleISBN(Isbn isbn) {
        try {
            return (Book) entityManager.createQuery("from Book where isbn = '" + isbn + "'").getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Book> findBooksByAuthor(String author) {
        List<Book> booksname = getBooksFromQuery("from Book where name like '%" + author + "%'");
        List<Book> booksfirstname = getBooksFromQuery("from Book where firstname like '%" + author + "%'");
        for (Book b : booksname) {
            if (!booksfirstname.contains(b)) {
                booksfirstname.add(b);
            }
        }
        return booksfirstname;
    }

    public List<Book> findBooksOnTitle(String title) {
        if (title != null) {
            if (title.equals("*")) {
                return findAllBooks();
            }
        }
        return getBooksFromQuery("from Book where title like '%" + title + "%'");
    }

}
