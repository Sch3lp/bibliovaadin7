package com.cegeka.bibliothouris.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class LoanRepository extends AbstractRepository<Loan> {

    @SuppressWarnings("unchecked")
    private List<Loan> getLoansFromQuery(String query) {
        return entityManager.createQuery(query).getResultList();
    }

    @SuppressWarnings("unchecked")
    private List<String> getStringFromQuery(String query) {
        return entityManager.createQuery(query).getResultList();
    }

    public List<Loan> findAll() {
        return getLoansFromQuery("from Loan");
    }

    public Loan findLoanOnIsbn(Isbn isbn) {
        try {
            return (Loan) entityManager.createQuery("from Loan where isbn = '" + isbn + "'").getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void remove(Loan loan) {
        entityManager.remove(entityManager.merge(loan));
    }

    public List<Isbn> findAllIsbnFromMemberInsz(String insz) {
        List<Isbn> listWithIsbn = new ArrayList<Isbn>();
        for (Object isbnNumber : entityManager.createQuery("select isbn from Loan where memberInsz = '" + insz + "'")
            .getResultList()) {
            listWithIsbn.add((Isbn) isbnNumber);
        }
        return listWithIsbn;
    }

    public List<String> getMembersInszFromLoan() {
        return getStringFromQuery("select memberInsz from Loan");
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getFinesFromMemberInsz(String insz) {
        return entityManager.createQuery("select fine from Loan where memberInsz = '" + insz + "'").getResultList();
    }

}
