package com.cegeka.bibliothouris.domain;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class MemberRepository extends AbstractRepository<Member> {

    @SuppressWarnings("unchecked")
    public List<Member> findAll() {
        return entityManager.createQuery("from Member").getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<String> findAllInsz() {
        return entityManager.createQuery("select insz from Member").getResultList();
    }

    public Member getMemberOnInsz(String insz) {
        try {
            return (Member) entityManager.createQuery("from Member where insz = '" + insz + "'").getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
