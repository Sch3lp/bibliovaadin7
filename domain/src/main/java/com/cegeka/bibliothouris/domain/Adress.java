package com.cegeka.bibliothouris.domain;

import javax.persistence.Embeddable;

@Embeddable
public class Adress {

    private String street, city, housenumber, postalcode;

    public Adress() {
    }

    public Adress(String street, String housenumber, String postalcode, String city) {
        setStreet(street);
        setHousenumber(housenumber);
        setPostalcode(postalcode);
        setCity(city);
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        if (housenumber.equals("")) {
            this.housenumber = null;
        } else {
            this.housenumber = housenumber;
        }
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        if (postalcode.equals("")) {
            this.postalcode = null;
        } else {
            this.postalcode = postalcode;
        }
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
