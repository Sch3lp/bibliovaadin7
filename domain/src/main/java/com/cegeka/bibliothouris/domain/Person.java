package com.cegeka.bibliothouris.domain;

import javax.persistence.Embeddable;

@Embeddable
public class Person {

    private String name, firstname;

    public Person() {
    }

    public Person(String name, String firstname) {
        setFirstname(firstname);
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String lastname) {
        this.name = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


}
