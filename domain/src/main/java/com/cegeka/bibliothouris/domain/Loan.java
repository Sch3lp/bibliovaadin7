package com.cegeka.bibliothouris.domain;

import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
public class Loan extends com.cegeka.bibliothouris.infrastructure.Entity {

    @Embedded
    private Isbn isbn;

    private String memberInsz;

    private Date startDate, endDate;

    private int fine;

    public Loan() {
        setStartDate(Time.getTodayTime());
    }

    public Loan(Isbn bookIsbn, String memberInsz, Date startDate, Date endDate) {
        setIsbn(bookIsbn);
        setMemberInsz(memberInsz);
        setStartDate(startDate);
        setEndDate(endDate);
        calculateFine();
    }

    public Loan(String bookIsbn, String memberInsz, String endDate) {
        setStartDate(Time.getTodayTime());
        setIsbn(new Isbn(bookIsbn));
        setMemberInsz(memberInsz);
        setEndDate(new Date(Time.convertStringDateToLong(endDate)));
    }

    public int getFine() {
        return fine;
    }

    public void setFine(int fine) {
        this.fine = fine;
    }

    public Isbn getIsbn() {
        return isbn;
    }

    public void setIsbn(Isbn bookIsbn) {
        this.isbn = bookIsbn;
    }

    public String getMemberInsz() {
        return memberInsz;
    }

    public void setMemberInsz(String memberInsz) {
        this.memberInsz = memberInsz;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + fine;
        result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
        result = prime * result + ((memberInsz == null) ? 0 : memberInsz.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Loan)) {
            return false;
        }
        Loan loan = (Loan) obj;
        return this.getEndDate().equals(loan.getEndDate()) && this.getFine() == loan.getFine()
                && this.getIsbn().getIsbnValue().equals(loan.getIsbn().getIsbnValue())
                && this.getMemberInsz().equals(loan.getMemberInsz()) && this.getStartDate().equals(loan.getStartDate());
    }

    public Boolean isBookReturnedBeforeEndDate() {
        return Time.getTodayTime().before(getEndDate());
    }

    public int calculateFine() {
        if (!isBookReturnedBeforeEndDate()) {
            createFine();
        } else {
            setFine(0);
        }
        return getFine();
    }

    private void createFine() {
        long dayInMilliSeconds = 1000 * 60 * 60 * 24;
        long numberOfDays = ((Time.getTodayTime().getTime() - getEndDate().getTime()) / dayInMilliSeconds);
        if (numberOfDays > 21) {
            setFine(5);
            while (numberOfDays > 28) {
                setFine(getFine() + 2);
                numberOfDays -= 7;
            }
        }
    }
}
