package com.cegeka.bibliothouris.domain;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class Isbn {

    private String isbn;

    @Transient
    private int checkIsbnDigit;

    @Transient
    private int lastNumberFromIsbn;

    @Transient
    private int correctLastNumber;

    public Isbn() {
    }

    public Isbn(String isbn) {
        setIsbnValue(isbn);
    }


    public String getIsbnValue() {
        return isbn;
    }


    public void setIsbnValue(String isbn) {
        this.isbn = isbn;
    }


    public int getCheckIsbnDigit() {
        return checkIsbnDigit;
    }


    protected void setCheckIsbnDigit(int checkIsbnDigit) {
        this.checkIsbnDigit = checkIsbnDigit;
    }


    public int getLastNumberFromIsbn() {
        return lastNumberFromIsbn;
    }


    protected void setLastNumberFromIsbn(int lastNumberFromIsbn) {
        this.lastNumberFromIsbn = lastNumberFromIsbn;
    }


    public int getCorrectLastNumber() {
        return correctLastNumber;
    }


    protected void setCorrectLastNumber(int correctLastNumber) {
        this.correctLastNumber = correctLastNumber;
    }

    @Override
    public String toString() {
        return getIsbnValue();
    }

    public boolean isLastNumberValid() {
        boolean isValid = true;
        if (isIsbnFormatValid()) {
            int step = 0;
            while ((getCheckIsbnDigit() % 100) % 10 != 0) {
                setCheckIsbnDigit(getCheckIsbnDigit() + 1);
                step++;
            }
            if (step != getLastNumberFromIsbn()) {
                setCorrectLastNumber(step);
                isValid = false;
            }
        }
        return isValid;
    }

    public boolean isIsbnFormatValid() {
        boolean isValid = false;
        String[] pieces = this.getIsbnValue().split("-");
        try {
            int firstNumber = Integer.parseInt(pieces[0]) / 100;
            int secondNumber = (Integer.parseInt(pieces[0]) % 100) / 10;
            int thirdNumber = (Integer.parseInt(pieces[0]) % 100) % 10;
            int fourthNumber = Integer.parseInt(pieces[1]) / 10;
            int fifthNumber = Integer.parseInt(pieces[1]) % 10;
            int sixthNumber = Integer.parseInt(pieces[2]) / 100;
            int seventhNumber = (Integer.parseInt(pieces[2]) % 100) / 10;
            int eigthNumber = (Integer.parseInt(pieces[2]) % 100) % 10;
            int ninthNumber = Integer.parseInt(pieces[3]) / 1000;
            int tenthNumber = (Integer.parseInt(pieces[3]) % 1000) / 100;
            int eleventhNumber = ((Integer.parseInt(pieces[3]) % 1000) % 100) / 10;
            int twelvethNumber = ((Integer.parseInt(pieces[3]) % 1000) % 100) % 10;
            this.setLastNumberFromIsbn(Integer.parseInt(pieces[4]));
            this.setCheckIsbnDigit(firstNumber + thirdNumber + fifthNumber + seventhNumber + ninthNumber
                    + eleventhNumber
                    + (secondNumber + fourthNumber + sixthNumber + eigthNumber + tenthNumber + twelvethNumber) * 3);
            isValid = true;
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;
    }

    public boolean isIsbnInRightFormat() {
        return (this.getIsbnValue().charAt(3) == '-') && (this.getIsbnValue().charAt(6) == '-')
                && (this.getIsbnValue().charAt(15) == '-');
    }

    public boolean isIsbn17Characters() {
        return isbn != null && this.getIsbnValue().length() == 17;
    }

    public boolean isIsbnNull() {
        return isbn == null;
    }

    public boolean isIsbnValidWithAllTheChecks() {
        return (!isIsbnNull() && isIsbn17Characters() && isIsbnFormatValid() && isIsbnInRightFormat() && isLastNumberValid());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + checkIsbnDigit;
        result = prime * result + correctLastNumber;
        result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
        result = prime * result + lastNumberFromIsbn;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Isbn)) {
            return false;
        }
        Isbn isbn = (Isbn) obj;
        return this.getIsbnValue().equals(isbn.getIsbnValue());
    }


}
