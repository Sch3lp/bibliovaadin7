package com.cegeka.bibliothouris.domain;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
public class Member extends com.cegeka.bibliothouris.infrastructure.Entity {

    private String insz;

    @Embedded
    private Adress adress;

    @Embedded
    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Transient
    private int fine;

    public Member() {
    }

    public Member(String insz, Person person, Adress adress) {
        setInsz(insz);
        setPerson(person);
        setAdress(adress);
    }


    public Adress getAdress() {
        return adress;
    }


    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    public int getFine() {
        return fine;
    }

    public void setFine(int fine) {
        this.fine = fine;
    }

    public String getInsz() {
        return insz;
    }

    public void setInsz(String insz) {
        this.insz = insz;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((insz == null) ? 0 : insz.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Member)) {
            return false;
        }
        Member member = (Member) obj;
        return this.getInsz().equals(member.getInsz());
    }


}
