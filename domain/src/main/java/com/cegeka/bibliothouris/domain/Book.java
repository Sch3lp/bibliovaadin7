package com.cegeka.bibliothouris.domain;

import javax.persistence.Embedded;
import javax.persistence.Entity;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
public class Book extends com.cegeka.bibliothouris.infrastructure.Entity {

    @Embedded
    private Isbn isbn;

    @Embedded
    private Person author;

    private String title;

    public Book() {
    }

    public Book(Isbn isbn, String title, Person author) {
        setIsbn(isbn);
        setTitle(title);
        setAuthor(author);
    }

    public Person getAuthor() {
        return author;
    }


    public void setAuthor(Person author) {
        this.author = author;
    }

    public Isbn getIsbn() {
        return isbn;
    }

    public void setIsbn(Isbn isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Book)) {
            return false;
        }
        Book book = (Book) obj;
        return this.getIsbn().getIsbnValue().equals(book.getIsbn().getIsbnValue());
    }


}