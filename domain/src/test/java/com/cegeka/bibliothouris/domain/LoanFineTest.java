package com.cegeka.bibliothouris.domain;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.cegeka.bibliothouris.test.TestCase;


public class LoanFineTest extends TestCase {

    private Book book;

    private Member member;

    private Loan loan;

    private Date dateLessThan21DaysFromToday, dateBetween21and28daysFromToday, dateBetween28and35daysFromToday,
            dateOver35DaysFromToday;

    private Isbn isbn;

    private Person person1, person2;

    private Adress adress;

    @Before
    public void setUp() {
        isbn = new Isbn("333-00-000-0000-5");
        person1 = new Person("Test", "Johny");
        person2 = new Person("Wallenus", "Jeroen");
        adress = new Adress("Gemeentestraat", "132", "3210", "Linden");
        dateLessThan21DaysFromToday = new Date(Time.getTodayTime().getTime() - (1000 * 60 * 60 * 24 * 21));
        dateBetween21and28daysFromToday = new Date(dateLessThan21DaysFromToday.getTime() - (1000 * 60 * 60 * 24 * 7));
        dateBetween28and35daysFromToday = new Date(dateBetween21and28daysFromToday.getTime()
                - (1000 * 60 * 60 * 24 * 7));
        dateOver35DaysFromToday = new Date(dateBetween28and35daysFromToday.getTime() - (1000 * 60 * 60 * 24 * 7));
        book = new Book(isbn, "TestBook", person1);
        member = new Member("31", person2, adress);
        loan = new Loan(book.getIsbn(), member.getInsz(), Time.getTodayTime(), dateLessThan21DaysFromToday);
    }

    @Test
    public void testReturnInTime() {
        assertEquals(0, loan.calculateFine());
    }

    @Test
    public void testReturn3WeeksToLate() {
        loan.setEndDate(dateBetween21and28daysFromToday);
        assertEquals(5, loan.calculateFine());
    }

    @Test
    public void testReturn4WeeksToLate() {
        loan.setEndDate(dateBetween28and35daysFromToday);
        assertEquals(7, loan.calculateFine());
    }

    @Test
    public void testReturn5WeeksToLate() {
        loan.setEndDate(dateOver35DaysFromToday);
        assertEquals(9, loan.calculateFine());
    }


}
