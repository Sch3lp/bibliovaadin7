package com.cegeka.bibliothouris.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.cegeka.bibliothouris.test.TestCase;


public class IsbnIsValidTest extends TestCase {

    private Isbn isbn;

    @Before
    public void setUp() {
        isbn = new Isbn();
        isbn.setIsbnValue("123-45-678-9012-8");
    }

    @Test
    public void testIsValidWithNormalFormat() {
        assertTrue(isbn.isIsbn17Characters());
        assertTrue(isbn.isIsbnFormatValid());
        assertTrue(isbn.isIsbnInRightFormat());
        assertTrue(isbn.isLastNumberValid());
    }

    @Test
    public void testIsValidWithNoInput() {
        isbn.setIsbnValue("");
        assertFalse(isbn.isIsbn17Characters());
    }

    @Test
    public void testIsValidWithNullInput() {
        isbn.setIsbnValue(null);
        assertFalse(isbn.isIsbn17Characters());
    }

    @Test
    public void testIsValidTooLongIsbn() {
        isbn.setIsbnValue("978-90-274-3964-23");
        assertFalse(isbn.isIsbn17Characters());
    }

    @Test
    public void testIsValidWrongFormat() {
        isbn.setIsbnValue("98-190-274-3964-2");
        assertFalse(isbn.isIsbnInRightFormat());
    }

    @Test
    public void testIsValidWithAlphabeticalTokens() {
        isbn.setIsbnValue("12e-45-678-9012-8");
        assertFalse(isbn.isIsbnFormatValid());
    }

    @Test
    public void testWithWrongCheckDigit() {
        isbn.setIsbnValue("123-45-678-9012-9");
        assertFalse(isbn.isLastNumberValid());
    }

}
