package com.cegeka.bibliothouris.domain;


public class BookTestBuilder {

    public static final Isbn ISBN = new Isbn("330-00-000-0000-8");

    public static final String TITLE = "SpringInAction";

    public static final Person PERSON = new Person("Wallenus", "Jeroen");


    private Isbn isbn = ISBN;

    private String title = TITLE;

    private Person person = PERSON;

    public Book build() {
        return new Book(isbn, title, person);
    }

    public BookTestBuilder withIsbn(Isbn isbn) {
        this.isbn = isbn;
        return this;
    }

    public BookTestBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public BookTestBuilder withPerson(Person person) {
        this.person = person;
        return this;
    }

}
