package com.cegeka.bibliothouris.domain;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import com.cegeka.bibliothouris.test.IntegrationTestCase;


public class MemberRepositoryTest extends IntegrationTestCase {

    @Inject
    private MemberRepository memberRepository;

    private Member member1, member2, member3;

    private List<Integer> listWithInszNumbers;

    private Adress adress1, adress2, adress3;

    private Person person1, person2, person3;

    @Before
    public void setUp() {
        adress1 = new Adress("Z", "11", "3210", "E");
        adress2 = new Adress("Street", "23", "3222", "Brussel");
        adress3 = new Adress("Pol", "3", "3", "Zee");
        person1 = new Person("Q", "A");
        person2 = new Person("Name", "FName");
        person3 = new Person("Pol", "Pol");
        member1 = new Member("343", person1, adress1);
        member2 = new Member("434", person2, adress2);
        member3 = new Member("334", person3, adress3);
        listWithInszNumbers = new ArrayList<Integer>();
        listWithInszNumbers.add(343);
        listWithInszNumbers.add(434);
        listWithInszNumbers.add(334);
    }

    @Test
    public void canStoreMemberTest() {
        memberRepository.store(member1);
        flushAndClear();
        assertTrue(member1.equals(entityManager.find(Member.class, member1.getId())));
    }

    @Test
    public void canFindAll() {
        memberRepository.store(member1);
        memberRepository.store(member2);
        flushAndClear();
        List<Member> result = memberRepository.findAll();
        assertEquals(2, result.size());
        assertTrue(member1.equals(result.get(0)));
        assertTrue(member2.equals(result.get(1)));
    }

    @Test
    public void canFindInsz() {
        memberRepository.store(member1);
        memberRepository.store(member2);
        memberRepository.store(member3);
        flushAndClear();
        assertTrue(new ReflectionEquals(listWithInszNumbers).matches(memberRepository.findAllInsz()));
    }


}
