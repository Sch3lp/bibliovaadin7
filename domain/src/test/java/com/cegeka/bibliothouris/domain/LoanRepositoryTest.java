package com.cegeka.bibliothouris.domain;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import com.cegeka.bibliothouris.test.IntegrationTestCase;


public class LoanRepositoryTest extends IntegrationTestCase {

    @Inject
    private LoanRepository loanRepository;

    @Inject
    private BookRepository bookRepository;

    @Inject
    private MemberRepository memberRepository;

    private Loan loan1, loan2, loan3;

    private Book book1, book2;

    private Member member1, member2;

    private Person person1, person2;

    private Adress adress1, adress2;

    private List<String> listWithIsbnFromMember1, listWithIsbnFromMember2;

    @Before
    public void setUp() {
        person1 = new Person("Detoffe", "Thomas");
        person2 = new Person("Deplezante", "Stijn");
        adress1 = new Adress("holastraat", "15", "6832", "Leuven");
        adress2 = new Adress("Brusselsesteenweg", "57", "3090", "Overijse");
        book1 = new BookTestBuilder().withIsbn(new Isbn("111-11-111-0000-4")).withPerson(new Person("Coninx", "Tom"))
            .withTitle("Ik ben een voetbalfan").build();
        book2 = new BookTestBuilder().withIsbn(new Isbn("111-11-111-1000-3"))
            .withPerson(new Person("Grootaerts", "Walter")).withTitle("Ik ben een zanger").build();
        member1 = new Member("23", person1, adress1);
        member2 = new Member("24", person2, adress2);
        memberRepository.store(member1);
        memberRepository.store(member2);
        bookRepository.store(book1);
        bookRepository.store(book2);
        flushAndClear();
        loan1 = new Loan(book1.getIsbn(), member1.getInsz(), new Date(Time.convertStringDateToLong("02-02-2011")),
            new Date(Time.convertStringDateToLong("23-02-2011")));
        loan2 = new Loan(book2.getIsbn(), member2.getInsz(), new Date(Time.convertStringDateToLong("09-01-2011")),
            new Date(Time.convertStringDateToLong("30-01-2011")));
        loan3 = new Loan(book1.getIsbn(), member2.getInsz(), new Date(Time.convertStringDateToLong("25-05-2011")),
            new Date(Time.convertStringDateToLong("01-01-2012")));
        listWithIsbnFromMember1 = new ArrayList<String>();
        listWithIsbnFromMember1.add(book1.getIsbn().getIsbnValue());
        listWithIsbnFromMember2 = new ArrayList<String>();
        listWithIsbnFromMember2.add(book2.getIsbn().getIsbnValue());
        listWithIsbnFromMember2.add(book1.getIsbn().getIsbnValue());
    }

    @Test
    public void canStoreLoanTest() {
        loanRepository.store(loan1);
        flushAndClear();
        assertTrue(loan1.equals(entityManager.find(Loan.class, loan1.getId())));
    }

    @Test
    public void canFindAll() {
        loanRepository.store(loan1);
        loanRepository.store(loan2);
        flushAndClear();
        List<Loan> result = loanRepository.findAll();
        assertEquals(2, result.size());
        assertTrue(loan1.equals(result.get(0)));
        assertTrue(loan2.equals(result.get(1)));
    }

    @Test
    public void canFindIsbnFromMemberInsz() {
        loanRepository.store(loan1);
        loanRepository.store(loan2);
        loanRepository.store(loan3);
        flushAndClear();
        List<Isbn> result = loanRepository.findAllIsbnFromMemberInsz(member2.getInsz());
        assertTrue(new ReflectionEquals(result).matches(listWithIsbnFromMember2));
        result = loanRepository.findAllIsbnFromMemberInsz(member1.getInsz());
        assertTrue(new ReflectionEquals(result).matches(listWithIsbnFromMember1));
    }

}
