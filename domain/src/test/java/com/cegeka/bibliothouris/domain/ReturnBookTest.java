package com.cegeka.bibliothouris.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.cegeka.bibliothouris.test.IntegrationTestCase;


public class ReturnBookTest extends IntegrationTestCase {

    private Loan loan1, loan2;

    private Book book1, book2;

    private Person person1;

    private Adress adress1;

    @Inject
    private LoanRepository loanRepository;

    @Inject
    private MemberRepository memberRepository;

    @Inject
    private BookRepository bookRepository;

    private Member member1;

    @Before
    public void setUp() {
        person1 = new Person("Detoffe", "Thomas");
        adress1 = new Adress("holastraat", "15", "6832", "Leuven");
        member1 = new Member("28", person1, adress1);
        book1 = new BookTestBuilder().withIsbn(new Isbn("111-11-111-0000-4")).withPerson(new Person("Coninx", "Tom"))
            .withTitle("Ik ben een voetbalfan").build();
        book2 = new BookTestBuilder().withIsbn(new Isbn("111-11-111-1000-3"))
            .withPerson(new Person("Grootaerts", "Walter")).withTitle("Ik ben een zanger").build();
        loan1 = new Loan(book2.getIsbn(), member1.getInsz(), new Date(Time.convertStringDateToLong("09-01-2011")),
            new Date(Time.convertStringDateToLong("30-01-2011")));
        loan2 = new Loan(book1.getIsbn(), member1.getInsz(), new Date(Time.convertStringDateToLong("19-03-2011")),
            new Date(Time.convertStringDateToLong("31-12-8000")));
        memberRepository.store(member1);
        bookRepository.store(book1);
        bookRepository.store(book2);
        loanRepository.store(loan1);
        loanRepository.store(loan2);
        flushAndClear();
    }

    @Test
    public void testReturnBookInTime() {
        assertFalse(loanRepository.findLoanOnIsbn(loan1.getIsbn()).isBookReturnedBeforeEndDate());
    }

    @Test
    public void testReturnBookNotInTime() {
        assertTrue(loanRepository.findLoanOnIsbn(loan2.getIsbn()).isBookReturnedBeforeEndDate());
        assertTrue(loanRepository.findLoanOnIsbn(loan2.getIsbn()).isBookReturnedBeforeEndDate());
    }
}
