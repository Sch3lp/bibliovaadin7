package com.cegeka.bibliothouris.domain;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import com.cegeka.bibliothouris.test.IntegrationTestCase;

public class BookRepositoryTest extends IntegrationTestCase {

    @Inject
    private BookRepository bookRepository;

    private Book book1, book2, book3, book4, book5, book6, book7;

    private Isbn isbn1, isbn2, isbn3, isbn4;

    private Person person1, person2, person3, person4;

    private List<Book> listWithOnlyWildcart, listWithoutWildcarts, listWithWildcartInBegin, listWithWildcartInEnd,
            listWithTwoWildcarts, listWithBook1and2, listWithBook1, listTitlesFromBooks;

    @Before
    public void setUp() {
        isbn1 = new Isbn("111-10-000-0000-2");
        isbn2 = new Isbn("111-11-000-0000-1");
        isbn3 = new Isbn("111-11-100-0000-8");
        isbn4 = new Isbn("111-11-110-0000-7");
        person1 = new Person("Naam 1", "Voornaam 1");
        person2 = new Person("Naam 2", "Voornaam 2");
        person3 = new Person("Naam 3", "Voornaam 3");
        person4 = new Person("Naam 4", "Voornaam 4");
        book7 = new BookTestBuilder().build();
        book1 = new BookTestBuilder().withIsbn(new Isbn("110-00-000-0000-6")).withTitle("Domain Driven Design")
            .withPerson(new Person("Vanoverloop", "Jonathan")).build();
        book2 = new BookTestBuilder().withIsbn(new Isbn("111-00-000-0000-5")).withTitle("Design Patterns")
            .withPerson(new Person("Wallenus", "Jeroen")).build();
        book3 = new Book(isbn1, "Title 1", person1);
        book4 = new Book(isbn2, "Title 2", person2);
        book5 = new Book(isbn3, "Title 3", person3);
        book6 = new Book(isbn4, "Title 4", person4);
        listWithoutWildcarts = new ArrayList<Book>();
        listWithWildcartInBegin = new ArrayList<Book>();
        listWithWildcartInEnd = new ArrayList<Book>();
        listWithTwoWildcarts = new ArrayList<Book>();
        listWithOnlyWildcart = new ArrayList<Book>();
        listTitlesFromBooks = new ArrayList<Book>();
        listWithoutWildcarts.add(book3);
        listWithWildcartInBegin.add(book5);
        listWithWildcartInEnd.add(book6);
        listWithTwoWildcarts.add(book3);
        listWithTwoWildcarts.add(book4);
        listWithTwoWildcarts.add(book5);
        listWithTwoWildcarts.add(book6);
        listWithOnlyWildcart.add(book3);
        listWithOnlyWildcart.add(book4);
        listWithOnlyWildcart.add(book5);
        listWithOnlyWildcart.add(book6);
        listWithBook1and2 = new ArrayList<Book>();
        listWithBook1and2.add(book1);
        listWithBook1and2.add(book2);
        listWithBook1 = new ArrayList<Book>();
        listWithBook1.add(book1);
        listTitlesFromBooks.add(book1);
        listTitlesFromBooks.add(book2);

    }

    @Test
    public void canStoreBook() {
        bookRepository.store(book7);
        flushAndClear();
        assertTrue(book7.equals(entityManager.find(Book.class, book7.getId())));
    }

    @Test
    public void canFindAll() {
        persistFlushAndClear(book1, book2);
        List<Book> result = bookRepository.findAllBooks();
        assertEquals(2, result.size());
        assertTrue(book1.equals(result.get(0)));
        assertTrue(book2.equals(result.get(1)));
    }

    @Test
    public void canSearchOnISBN() {
        persistFlushAndClear(book3, book4, book5, book6);
        assertTrue(new ReflectionEquals(listWithoutWildcarts).matches(bookRepository.findBooksByISBN(new Isbn(
            "111-10-000-0000-2"))));
        assertTrue(new ReflectionEquals(listWithTwoWildcarts).matches(bookRepository.findBooksByISBN(new Isbn("*0*"))));
        assertTrue(new ReflectionEquals(listWithWildcartInBegin)
            .matches(bookRepository.findBooksByISBN(new Isbn("*8"))));
        assertTrue(new ReflectionEquals(listWithWildcartInEnd).matches(bookRepository.findBooksByISBN(new Isbn(
            "111-10*"))));
        assertTrue(new ReflectionEquals(listWithOnlyWildcart).matches(bookRepository.findBooksByISBN(new Isbn("*"))));
    }

    @Test
    public void canSearchOnAuthor() {
        persistFlushAndClear(book1, book2);
        assertTrue(new ReflectionEquals(listWithBook1and2).matches(bookRepository.findBooksByAuthor("J")));
        assertTrue(new ReflectionEquals(listWithBook1).matches(bookRepository.findBooksByAuthor("th")));
    }

    @Test
    public void canFindIsbn() {
        persistFlushAndClear(book1, book2);
        assertTrue(book1.equals(bookRepository.findBookBySingleISBN(new Isbn("110-00-000-0000-6"))));
        assertTrue(book2.equals(bookRepository.findBookBySingleISBN(new Isbn("111-00-000-0000-5"))));
    }

    @Test
    public void canFindOnTitle() {
        persistFlushAndClear(book1, book2);
        assertTrue(new ReflectionEquals(listTitlesFromBooks).matches(bookRepository.findBooksOnTitle("Design")));
    }
}
