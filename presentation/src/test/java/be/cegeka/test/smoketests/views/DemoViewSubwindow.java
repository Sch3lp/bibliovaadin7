package be.cegeka.test.smoketests.views;

import be.cegeka.test.vaadin.Button;
import be.cegeka.test.vaadin.Label;
import be.cegeka.test.vaadin.SubWindowBase;
import be.cegeka.test.vaadin.TextField;


public class DemoViewSubwindow extends SubWindowBase<DemoViewSubwindow, BiblioVaadinMainWindow> {

    public final Label<DemoViewSubwindow> demoLabel;

    public final Button<DemoViewSubwindow> demoButton;

    public final TextField<DemoViewSubwindow> demoTextField;

    public DemoViewSubwindow(BiblioVaadinMainWindow parentWindow) {
        super(parentWindow, "n-demoview");
        demoLabel = new Label<DemoViewSubwindow>(this, "n-demolabel");
        demoButton = new Button<DemoViewSubwindow>(this, "n-demobutton");
        demoTextField = new TextField<DemoViewSubwindow>(this, "n-demotextfield");
    }
}
