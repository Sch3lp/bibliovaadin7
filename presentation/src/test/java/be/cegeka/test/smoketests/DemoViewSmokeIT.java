package be.cegeka.test.smoketests;

import static be.cegeka.test.vaadin.VaadinTestBuilder.DEFAULT_SHUTDOWN_PORT;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import be.cegeka.jetty.JettyServer;
import be.cegeka.test.smoketests.views.BiblioVaadinMainWindow;
import be.cegeka.test.vaadin.VaadinTestBuilder;
import be.cegeka.test.vaadin.VaadinTestBuilder.Browser;


public class DemoViewSmokeIT {

    protected static BiblioVaadinMainWindow mainWindow;

    public static void setUpWindow() {
        mainWindow = new VaadinTestBuilder() //
            .withBrowser(Browser.IE) //
            .withServerUrl("http://localhost:8085") //
            .withDefaultTimeoutInSeconds(4)//
            .build(BiblioVaadinMainWindow.class);
    }

    @BeforeClass
    public static void setUp() {
        setUpWindow();
    }

    @AfterClass
    public static void tearDown() {
        mainWindow.close();
        JettyServer.stop(DEFAULT_SHUTDOWN_PORT);
    }

    @Test
    public void testDemoScreen() {
        mainWindow.onDemoViewSubwindow()//
        .demoLabel.assertText("DemoLabel")//
        .demoTextField.assertText("democontent")//
        .demoButton.clickAndWait()//
        .demoTextField.assertText("This is a buttonclick demonstration!");
    }
}
