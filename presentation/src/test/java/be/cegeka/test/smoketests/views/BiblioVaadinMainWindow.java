package be.cegeka.test.smoketests.views;

import be.cegeka.test.vaadin.MainWindowBase;


public class BiblioVaadinMainWindow extends MainWindowBase<BiblioVaadinMainWindow> {

    public BiblioVaadinMainWindow() {
        super("");
    }

    public DemoViewSubwindow onDemoViewSubwindow() {
        return new DemoViewSubwindow(this);
    }
}
