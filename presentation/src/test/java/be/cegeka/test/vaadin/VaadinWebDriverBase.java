package be.cegeka.test.vaadin;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class VaadinWebDriverBase {

    public enum Resolve {
        EAGER, LAZY
    }

    protected boolean hasStyle(WebElement component, String styleName) {
        String styles = component.getAttribute("style");
        if (styles == null) {
            return false;
        }

        for (String foundStyle : styles.split(";")) {
            if (styleName.replace(" ", "").equalsIgnoreCase(foundStyle.replace(" ", ""))) {
                return true;
            }
        }
        return false;
    }

    protected boolean hasClass(WebElement component, String className) {
        String classes = component.getAttribute("class");
        if (classes == null) {
            return false;
        }

        for (String foundClassName : classes.split(" ")) {
            if (className.equalsIgnoreCase(foundClassName)) {
                return true;
            }
        }
        return false;
    }

    protected boolean hasVisibleElementWithId(SearchContext context, String id) {
        List<WebElement> elements = context.findElements(By.id(id));
        if (elements.size() == 0) {
            return false;
        }
        return isVisible(elements.get(0));

    }

    protected boolean isVisible(WebElement element) {
        return element.isDisplayed();
    }

    protected void assertWithTimeout(final WebDriver driver, int timeout, final Runnable runnable) {
        if (timeout <= 0) {
            runnable.run();
            return;
        }

        final List<Error> errors = new ArrayList<Error>();
        try {
            (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {

                @Override
                public Boolean apply(WebDriver d) {
                    try {
                        runnable.run();
                        return true;
                    } catch (AssertionError e) {
                        errors.clear();
                        errors.add(e);
                        return false;
                    }
                }
            });
        } catch (TimeoutException e) {
            e.printStackTrace();
            throw errors.get(0);
        }
    }
}
