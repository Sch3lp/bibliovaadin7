package be.cegeka.test.vaadin;

import org.junit.Assert;
import org.openqa.selenium.By;

public class Button<T extends WindowBase<T>> extends Component<T> {

    public Button(T window, String id) {
        super(window, id);
    }

    public Button(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    public T click() {
        component().click();
        return window;
    }

    public T clickAndWait() {
        T click = click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        return click;
    }

    @Override
    public void assertTextInternal(String text) {
        Assert.assertEquals(text, getButtonCaption());
    }

    private String getButtonCaption() {
        return component().findElement(By.className("v-button-caption")).getText();
    }
}
