package be.cegeka.test.vaadin;

import static java.lang.Boolean.parseBoolean;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class CheckBox<T extends WindowBase<T>> extends Component<T> {

    public CheckBox(T window, String id) {
        super(window, id);
    }

    public CheckBox(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    public CheckBox(T window, WebElement cell) {
        super(window, cell);
    }

    public T assertChecked(boolean checked) {
        boolean actual = parseBoolean(component().findElement(By.tagName("input")).getAttribute("checked"));
        Assert.assertEquals(checked, actual);
        return window;
    }

    public T toggle() {
        findTag(component(), "input").click();
        return window;
    }

}
