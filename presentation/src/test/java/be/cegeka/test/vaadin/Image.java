package be.cegeka.test.vaadin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Image<T extends WindowBase<T>> extends Component<T> {

    public Image(T window, String id) {
        super(window, id);
    }

    public Image(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    public Image(T window, WebElement cell) {
        super(window, cell/* .findElement(By.tagName("img")) */);
    }

    public T click() {
        component().findElement(By.tagName("img")).click();
        return window;
    }
}
