package be.cegeka.test.vaadin;

import java.util.List;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class MenuBar<T extends WindowBase<T>> extends Component<T> {

    public MenuBar(T window) {
        super(window, (String) null);
    }

    public MenuBar(T window, VaadinWebDriverBase.Resolve resolve) {
        super(window, null, resolve);
    }

    public T assertMenuItemIsDisabled(String item) {
        List<WebElement> disabledMenuItems = window.search().findElements(By.className("v-menubar-menuitem-disabled"));
        for (WebElement menuItem : disabledMenuItems) {
            if (item.equals(menuItem.getText())) {
                return window;
            }
        }
        Assert.fail("Could not find menu item '" + item + "'. Or item is enabled.");
        return null;
    }

    public WebElement getMenuItem(String displayText) {
        WebElement menuBar = window.search().findElement(By.className("v-menubar"));
        List<WebElement> menuItems = menuBar.findElements(By.className("v-menubar-menuitem-caption"));
        for (WebElement webElement : menuItems) {
            if (webElement.getText().equals(displayText)) {
                return webElement;
            }
        }
        Assert.fail("Could not find menu item '" + displayText + "'");
        return null;
    }

    public T clickMenuItem(String... items) {
        if (items.length < 1 || items.length > 2) {
            throw new IllegalArgumentException("MenuBar.clickMenuItem() currently only supports 1 or 2 levels");
        }

        WebElement menuBar = window.search().findElement(By.className("v-menubar"));
        List<WebElement> menuItems = menuBar.findElements(By.className("v-menubar-menuitem-caption"));

        for (WebElement menuItem : menuItems) {
            if (items[0].equals(menuItem.getText())) {
                menuItem.click();

                if (items.length > 1) {
                    WebElement subMenu = window.search().findElement(By.className("v-menubar-submenu"));
                    List<WebElement> subMenuItems = subMenu.findElements(By.className("v-menubar-menuitem-caption"));
                    for (WebElement subMenuItem : subMenuItems) {
                        if (items[1].equals(subMenuItem.getText())) {
                            subMenuItem.click();
                            return window;
                        }
                    }
                    Assert.fail("Could not find menu item '" + items[1] + "'");
                }
                return window;
            }
        }
        Assert.fail("Could not find menu item '" + items[0] + "'");
        return null;
    }
}
