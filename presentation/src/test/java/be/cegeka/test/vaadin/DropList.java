package be.cegeka.test.vaadin;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class DropList<T extends WindowBase<T>> extends Component<T> {

    public DropList(T window, String id) {
        super(window, id);
    }

    public DropList(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    protected DropList(T window, WebElement cell) {
        super(window, cell);
    }

    public T selectText(String text) {
        findSelectElement().selectByVisibleText(text);
        return window;
    }

    @Override
    protected void assertTextInternal(String text) {
        Assert.assertEquals(text, findSelectElement().getFirstSelectedOption().getText());
    }

    private Select findSelectElement() {
        return new Select(findTag(component(), "select"));
    }
}
