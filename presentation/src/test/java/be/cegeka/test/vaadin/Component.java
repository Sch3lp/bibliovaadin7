package be.cegeka.test.vaadin;

import static be.cegeka.test.vaadin.VaadinWebDriverBase.Resolve.EAGER;
import static be.cegeka.test.vaadin.VaadinWebDriverBase.Resolve.LAZY;

import java.util.NoSuchElementException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Component<T extends WindowBase<T>> extends VaadinWebDriverBase {

    protected T window;

    protected String id;

    private WebElement element;

    protected Component(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        this.window = window;
        this.id = id;

        if (resolve == EAGER) {
            component();
        }
    }

    protected Component(T window, String id) {
        this(window, id, LAZY);
    }

    protected Component(T window, WebElement element) {
        this(window, (String) null);
        this.element = element;
    }

    protected WebElement component() {
        if (element == null) {
            try {
                element = window.search().findElement(By.id(id));
            } catch (NoSuchElementException e) {
                Assert.fail(getClass().getSimpleName() + " " + id + " not found");
            }
        }
        return element;
    }

    public final T assertEnabled(boolean enabled) {
        return assertEnabled(enabled, window.getDefaultTimeout());
    }

    public final T assertEnabled(final boolean enabled, int maxDelay) {
        assertWithTimeout(window.getDriver(), maxDelay, new Runnable() {

            @Override
            public void run() {
                assertEnabledInternal(enabled);
            }
        });
        return window;
    }

    protected void assertEnabledInternal(boolean enabled) {
        boolean isEnabled = !hasClass(component(), "v-disabled");
        Assert.assertEquals(getClass().getSimpleName() + " " + id + " is " + (isEnabled ? "enabled" : "disabled"),
            enabled, isEnabled);
    }

    public final T assertText(String text) {
        return assertText(text, window.getDefaultTimeout());
    }

    public final T assertText(final String text, int maxDelay) {
        assertWithTimeout(window.getDriver(), maxDelay, new Runnable() {

            @Override
            public void run() {
                assertTextInternal(text);
            }
        });
        return window;
    }

    protected void assertTextInternal(String text) {
        String actual = component().getText();
        Assert.assertEquals(getClass().getSimpleName() + " " + id + " expected text " + text + ", but was " + actual,
            text, actual);
    }

    public final T assertPresent(boolean present) {
        return assertPresent(present, window.getDefaultTimeout());
    }

    public final T assertPresent(final boolean present, int maxDelay) {
        assertWithTimeout(window.getDriver(), maxDelay, new Runnable() {

            @Override
            public void run() {
                assertPresentInternal(present);
            }
        });
        return window;
    }

    protected void assertPresentInternal(boolean present) {
        Assert.assertEquals(present, hasVisibleElementWithId(window.search(), id));
    }

    protected WebElement findTag(WebElement rootElement, String tag) {
        if (tag.equals(rootElement.getTagName())) {
            return rootElement;
        }
        return rootElement.findElement(By.tagName(tag));
    }

}
