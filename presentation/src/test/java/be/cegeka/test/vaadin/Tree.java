package be.cegeka.test.vaadin;


public class Tree<T extends WindowBase<T>> extends Component<T> {

    public Tree(T window, String id) {
        super(window, id);
    }

    public Tree(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    public T click() {
        component().click();
        return window;
    }

}
