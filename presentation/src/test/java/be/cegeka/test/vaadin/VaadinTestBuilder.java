package be.cegeka.test.vaadin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import be.cegeka.jetty.JettyServer;

public class VaadinTestBuilder {

    public static final int DEFAULT_PORT = 8085;

    public static final int DEFAULT_SHUTDOWN_PORT = 8095;

    private String url = "http://localhost:" + DEFAULT_PORT;

    private int defaultTimeout = 0;

    private Browser browser = Browser.FIREFOX;

    /**
     * IE setup: Internet options > Security > Internet AND Local intranet AND trusted sites AND Restricted sites >
     * Enable protected Mode (ON)
     */
    public enum Browser {
        FIREFOX(FirefoxDriver.class), CHROME(ChromeDriver.class), IE(InternetExplorerDriver.class);

        private Class<? extends WebDriver> driverClass;

        private Browser(Class<? extends WebDriver> driverClass) {
            this.driverClass = driverClass;
        }

        WebDriver getDriver() {
            try {
                return driverClass.newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public VaadinTestBuilder withBrowser(Browser browser) {
        this.browser = browser;
        return this;
    }

    public VaadinTestBuilder withServerUrl(String url) {
        this.url = url;
        return this;
    }

    public VaadinTestBuilder withDefaultTimeoutInSeconds(int defaultTimeout) {
        this.defaultTimeout = defaultTimeout;
        return this;
    }

    public <T extends MainWindowBase<T>> T build(Class<T> mainWindowClass) {
        try {
            JettyServer.stop(DEFAULT_SHUTDOWN_PORT);
            Thread.sleep(500);

            new JettyServer(DEFAULT_PORT).start();
            T mainWindow = mainWindowClass.newInstance();
            mainWindow.setServerUrl(url);
            mainWindow.setDriver(browser.getDriver());
            mainWindow.setDefaultTimeout(defaultTimeout);
            mainWindow.load();
            return mainWindow;
        } catch (Exception e) {
            throw new RuntimeException("Configuration error", e);
        }
    }
}
