package be.cegeka.test.vaadin;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;


public abstract class WindowBase<T extends WindowBase<T>> extends VaadinWebDriverBase {

    abstract SearchContext search();

    abstract WebDriver getDriver();

    abstract int getDefaultTimeout();

    protected void assertWithTimeout(final Runnable runnable) {
        assertWithTimeout(getDriver(), getDefaultTimeout(), runnable);
    }

}
