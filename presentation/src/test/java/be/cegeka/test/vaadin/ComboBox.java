package be.cegeka.test.vaadin;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class ComboBox<T extends WindowBase<T>> extends Component<T> {

    public ComboBox(T window, String id) {
        super(window, id);
    }

    public ComboBox(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    public T clearText() {
        getInputElement().clear();
        return window;
    }

    public T writeText(String text) {
        getInputElement().sendKeys(text);
        return window;
    }

    @Override
    public void assertTextInternal(String text) {
        String actual = component().findElement(By.tagName("input")).getAttribute("value");
        Assert.assertEquals(text, actual);
    }

    private WebElement getInputElement() {
        return component().findElement(By.className("v-filterselect-input"));
    }

    private WebElement getDropDown() {
        WebElement dropdown = null;
        try {
            dropdown = window.search().findElement(By.className("v-filterselect-suggestmenu"));
        } catch (NoSuchElementException e) {
            openDropDown();
            dropdown = window.search().findElement(By.className("v-filterselect-suggestmenu"));
        }
        return dropdown;
    }

    private void openDropDown() {
        component().findElement(By.className("v-filterselect-button")).click();
    }

    private List<String> getDropDownElements() {
        List<String> options = new ArrayList<String>();
        for (WebElement element : getDropDown().findElements(By.tagName("span"))) {
            if ("".equals(element.getText())) {
                continue;
            }
            options.add(element.getText());
        }
        return options;
    }

    protected void assertOptionsInternal(String... options) {
        List<String> actualOptions = getDropDownElements();
        Assert.assertEquals(getClass().getSimpleName() + " " + id + " expected " + options + " but was "
                + actualOptions, actualOptions.size(), options.length);
        for (String option : options) {
            Assert.assertTrue(getClass().getSimpleName() + " " + id + " Missing option: " + option,
                actualOptions.contains(option));
        }
    }

    public final T assertOptions(String... options) {
        return assertOptions(window.getDefaultTimeout(), options);
    }

    public final T assertOptions(int maxDelay, final String... options) {
        assertWithTimeout(window.getDriver(), maxDelay, new Runnable() {

            @Override
            public void run() {
                assertOptionsInternal(options);
            }
        });
        return window;
    }

}
