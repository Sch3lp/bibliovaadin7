package be.cegeka.test.vaadin;

import org.openqa.selenium.WebElement;


public class Label<T extends WindowBase<T>> extends Component<T> {

    public Label(T window, String id) {
        super(window, id);
    }

    public Label(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    public Label(T window, WebElement cell) {
        super(window, cell);
    }
}
