package be.cegeka.test.vaadin;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class TextField<T extends WindowBase<T>> extends Component<T> {

    public TextField(T window, String id) {
        super(window, id);
    }

    public TextField(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    protected TextField(T window, WebElement cell) {
        super(window, cell);
    }

    public T clearText() {
        getInputElement().clear();
        return window;
    }

    public T writeText(String text) {
        getInputElement().sendKeys(text);
        return window;
    }

    @Override
    public void assertTextInternal(String text) {
        Assert.assertEquals(text, getInputElement().getAttribute("value"));
    }

    private WebElement getInputElement() {
        return findTag(component(), "input");
    }

}
