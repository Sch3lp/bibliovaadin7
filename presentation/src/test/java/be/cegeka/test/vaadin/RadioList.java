package be.cegeka.test.vaadin;

import static java.lang.Boolean.parseBoolean;
import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class RadioList<T extends WindowBase<T>> extends Component<T> {

    public RadioList(T window, String id) {
        super(window, id);
    }

    public RadioList(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    public T assertOptionSelected(String item, boolean selected) {
        assertEquals(selected, parseBoolean(getOption(item).getAttribute("checked")));
        return window;
    }

    public T assertOptionEnabled(String item, boolean enabled) {
        assertEquals(!enabled, parseBoolean(getOption(item).getAttribute("disabled")));
        return window;
    }

    private WebElement getOption(String item) {
        for (WebElement option : component().findElements(By.className("v-radiobutton"))) {
            if (item.equals(option.findElement(By.tagName("label")).getText())) {
                return option.findElement(By.tagName("input"));
            }
        }
        Assert.fail("Option '" + item + "' not found");
        return null;
    }
}
