package be.cegeka.test.vaadin;

import static java.util.concurrent.TimeUnit.SECONDS;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;

public class MainWindowBase<T extends MainWindowBase<T>> extends WindowBase<T> {

    private WebDriver driver;

    private int defaultTimeout = 0;

    private String serverUrl;

    private String path;

    public MainWindowBase(String path) {
        this.path = path;
    }

    void load() {
        // attemp waiting a bit to prevent Jenkins hanging when the request is sent too soon
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }

        this.driver.manage().timeouts().implicitlyWait(defaultTimeout, SECONDS);
        this.driver.get(getServerUrl() + "/" + path);
    }

    @Override
    WebDriver getDriver() {
        return driver;
    }

    void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    void setDefaultTimeout(int defaultTimeout) {
        this.defaultTimeout = defaultTimeout;
    }

    void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    @Override
    protected int getDefaultTimeout() {
        return defaultTimeout;
    }

    protected String getServerUrl() {
        return serverUrl;
    }

    protected void setPath(String path) {
        this.path = path;
    }

    public void close() {
        driver.quit();
    }

    @Override
    protected SearchContext search() {
        return driver;
    }
}
