package be.cegeka.test.vaadin;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Popup<T extends MainWindowBase<T>> {

    private T parentWindow;

    private WebElement popupElement;

    public Popup(T parentWindow) {
        this.parentWindow = parentWindow;
        this.popupElement = parentWindow.getDriver().findElement(By.className("v-Notification"));
    }

    public T dismiss() {
        popupElement.click();
        (new WebDriverWait(parentWindow.getDriver(), 3)).until(new ExpectedCondition<Boolean>() {

            @Override
            public Boolean apply(WebDriver d) {
                return d.findElements(By.className("v-Notification")).size() == 0;
            }
        });
        return parentWindow;
    }

    public String getText() {
        return popupElement.findElement(By.tagName("h1")).getText();
    }

    public Popup<T> assertTextStartsWith(String text) {
        Assert.assertTrue("Popup text " + getText() + " does not start with " + text, getText().startsWith(text));
        return this;
    }
}
