package be.cegeka.test.vaadin;

import static junit.framework.Assert.assertFalse;

import java.io.File;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class FileUpload<T extends WindowBase<T>> extends Component<T> {

    public FileUpload(T window, String id) {
        super(window, id);
    }

    public FileUpload(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    public T sendFile(File file) {
        getFileElement().sendKeys(file.getAbsolutePath());

        waitForUploadToComplete();
        return window;
    }

    private void waitForUploadToComplete() {
        try { // make sure upload started
            Thread.sleep(200);
        } catch (InterruptedException e) {
        }

        assertWithTimeout(window.getDriver(), 60, new Runnable() {

            @SuppressWarnings("synthetic-access")
            @Override
            public void run() {
                assertFalse(isVisible(getProgressIndicator()));
            }
        });
    }

    private WebElement getFileElement() {
        return component().findElement(By.className("gwt-FileUpload"));
    }

    private WebElement getProgressIndicator() {
        return component().findElement(By.className("v-progressindicator"));
    }

    public T assertMessage(String message) {
        Assert.assertEquals(message, component().findElement(By.className("v-label")).getText());
        return window;
    }

}
