package be.cegeka.test.vaadin;

import java.util.List;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Table<T extends WindowBase<T>> extends Component<T> {

    public Table(T window, String id) {
        super(window, id);
    }

    public Table(T window, String id, VaadinWebDriverBase.Resolve resolve) {
        super(window, id, resolve);
    }

    public final T assertRowCount(int count) {
        return assertRowCount(count, window.getDefaultTimeout());
    }

    public final T assertRowCount(final int count, int maxDelay) {
        assertWithTimeout(window.getDriver(), maxDelay, new Runnable() {

            @SuppressWarnings("synthetic-access")
            @Override
            public void run() {
                Assert.assertEquals(count, getRows().size());
            }
        });
        return window;
    }

    private WebElement getRow(int row) {
        return getRows().get(row - 1);
    }

    private WebElement getFooterRow(int row) {
        return getFooterRows().get(row - 1);
    }

    private List<WebElement> getRows() {
        return component().findElement(By.className("v-table-body")).findElements(By.tagName("tr"));
    }

    private List<WebElement> getFooterRows() {
        return component().findElement(By.className("v-table-footer")).findElements(By.tagName("tr"));
    }

    private List<WebElement> getColumns(WebElement row) {
        return row.findElements(By.tagName("td"));
    }

    private WebElement getCell(int row, int column) {
        return getColumns(getRow(row)).get(column - 1);
    }

    private WebElement getFooterCell(int row, int column) {
        return getColumns(getFooterRow(row)).get(column - 1);
    }

    public T clickCell(int row, int column) {
        getCell(row, column).click();
        return window;
    }

    public T clickRow(int row) {
        return clickCell(row, 1);
    }

    public TextField<T> cellTextField(int row, int column) {
        return new TextField<T>(window, getCell(row, column));
    }

    public DropList<T> cellDropList(int row, int column) {
        return new DropList<T>(window, getCell(row, column));
    }

    public Image<T> cellImage(int row, int column) {
        return new Image<T>(window, getCell(row, column));
    }

    public CheckBox<T> cellCheckBox(int row, int column) {
        return new CheckBox<T>(window, getCell(row, column));
    }

    public Label<T> cellLabel(int row, int column) {
        return new Label<T>(window, getCell(row, column));
    }

    public Label<T> cellFooterLabel(int row, int column) {
        return new Label<T>(window, getFooterCell(row, column));
    }
}
