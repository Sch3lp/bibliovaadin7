package be.cegeka.test.vaadin;

import static be.cegeka.test.vaadin.VaadinWebDriverBase.Resolve.EAGER;
import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class SubWindowBase<T extends SubWindowBase<T, P>, P extends MainWindowBase<P>> extends WindowBase<T> {

    private WebElement windowElement;

    private P parentWindow;

    private String windowId;

    public SubWindowBase(P parentWindow, final String id) {
        this(parentWindow, id, EAGER);
    }

    public SubWindowBase(P parentWindow, final String id, Resolve resolve) {
        super();
        this.parentWindow = parentWindow;
        this.windowId = id;

        if (resolve == EAGER) {
            windowElement();
        }
    }

    protected WebElement windowElement() {
        if (windowElement == null) {
            assertWithTimeout(new Runnable() {

                @SuppressWarnings("synthetic-access")
                @Override
                public void run() {
                    windowElement = parentWindow.getDriver().findElement(By.id(windowId));
                }
            });
        }
        return windowElement;
    }

    @SuppressWarnings("unchecked")
    public T assertTitleBarText(String text) {
        Assert.assertEquals(text, search().findElement(By.className("v-window-header")).getText());
        return (T) this;
    }

    @Override
    protected SearchContext search() {
        return windowElement();
    }

    public P clickCloseInTitleBar() {
        search().findElement(By.className("v-window-closebox")).click();
        return parentWindow;
    }

    @Override
    WebDriver getDriver() {
        return parentWindow.getDriver();
    }

    @Override
    protected int getDefaultTimeout() {
        return parentWindow.getDefaultTimeout();
    }

    protected P getParentWindow() {
        return parentWindow;
    }
}
