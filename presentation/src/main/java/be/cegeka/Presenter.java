package be.cegeka;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.reflect.ConstructorUtils;

public abstract class Presenter<V extends View> {
	
	protected final V view;
	
	public Presenter(V view) {
		this.view = view;
	}
	
	protected void navigateTo(Screen screen, String... parameters) {
		String navigateTo = screen.getUrlFragment() + "/" + StringUtils.join(parameters, '/');
		view.getUI().getNavigator().navigateTo(navigateTo);
	}
	
	@SuppressWarnings("unchecked")
	static <V extends View> Presenter<V> createPresenter(V view) {
		Class<?> viewClass = view.getClass();
		try {
			Class<? extends Presenter<?>> presenterClass = Screen.forViewClass(viewClass).getPresenterClass();
			return (Presenter<V>) ConstructorUtils.invokeConstructor(presenterClass, view);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
