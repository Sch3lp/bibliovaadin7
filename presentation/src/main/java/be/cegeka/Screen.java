package be.cegeka;

import static com.google.common.base.Objects.equal;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Sets.newHashSet;

import java.util.HashSet;

import be.cegeka.bibliovaadin.detail.DetailPresenter;
import be.cegeka.bibliovaadin.detail.DetailView;
import be.cegeka.bibliovaadin.home.HomeMobileView;
import be.cegeka.bibliovaadin.home.HomePresenter;
import be.cegeka.bibliovaadin.home.HomeDesktopView;
import be.cegeka.bibliovaadin.insert.InsertPresenter;
import be.cegeka.bibliovaadin.insert.InsertView;

import com.google.common.base.Predicate;

public enum Screen {

	HOME(HomeDesktopView.class, HomeMobileView.class, HomePresenter.class, ""),
	DETAIL(DetailView.class, DetailView.class, DetailPresenter.class, "detail"),
	INSERT(InsertView.class, InsertView.class, InsertPresenter.class, "insert");
	
	private final Class<? extends Presenter<?>> presenterClass;
	private final String urlFragment;
	private Class<? extends View> viewClass;
	private Class<? extends View> mobileViewClass;

	private Screen(Class<? extends View> viewClass, Class<? extends View> mobileViewClass, Class<? extends Presenter<?>> presenterClass, String urlFragment) {
		this.viewClass = viewClass;
		this.mobileViewClass = mobileViewClass;
		this.presenterClass = presenterClass;
		this.urlFragment = urlFragment;
	}

	public Class<? extends View> getViewClass(boolean mobile) {
		if (mobile) {
			return mobileViewClass;
		}
		return viewClass;
	}

	public String getUrlFragment() {
		return urlFragment;
	}

	public Class<? extends Presenter<?>> getPresenterClass() {
		return presenterClass;
	}

	public static Screen forViewClass(Class<?> viewClass) {
		return find(valueSet(), hasViewClass(viewClass));
	}

	private static Predicate<Screen> hasViewClass(final Class<?> viewClass) {
		return new Predicate<Screen>() {
			public boolean apply(Screen input) {
				return equal(input.getViewClass(false), viewClass)
						|| equal(input.getViewClass(true), viewClass);
			}
		};
	}

	private static HashSet<Screen> valueSet() {
		return newHashSet(values());
	}

}
