package be.cegeka;

import static org.apache.commons.lang.StringUtils.containsIgnoreCase;

import org.apache.commons.lang.StringUtils;

import com.vaadin.navigator.Navigator.ClassBasedViewProvider;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;

public class CustomViewProvider implements ViewProvider {

	private BiblioVaadinUI ui;
	private Screen view;

	public CustomViewProvider(BiblioVaadinUI ui, Screen view) {
		this.ui = ui;
		this.view = view;
	}

	@Override
	public View getView(String viewName) {
		View view = getInternalViewProvider().getView(viewName);
		((be.cegeka.View) view).setErrorMessages(ui.getErrorMessages());
		return view;
	}

	@Override
	public String getViewName(String viewAndParameters) {
		return getInternalViewProvider().getViewName(viewAndParameters);
	}

	private ViewProvider getInternalViewProvider() {
		return new ClassBasedViewProvider(view.getUrlFragment(), view.getViewClass(isMobile()));
	}

	private boolean isMobile() {
		String userAgent = ui.getSession().getService().getCurrentRequest().getHeader("user-agent");
		return containsIgnoreCase(userAgent, "Mobile");
	}

}
