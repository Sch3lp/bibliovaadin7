package be.cegeka;

import static be.cegeka.Presenter.createPresenter;
import be.cegeka.bibliovaadin.component.ErrorMessages;
import be.cegeka.bibliovaadin.home.HomeDesktopView;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@Theme("vcctheme")
public class BiblioVaadinUI extends UI {

	private ErrorMessages errorMessages = new ErrorMessages();
	
	@Override
    public void init(VaadinRequest request) {
		
        setNavigator(new Navigator(this,this));
        for (Screen view : Screen.values()) {
//        	getNavigator().addProvider(new ClassBasedViewProvider(view.getUrlFragment(), view.getViewClass()));
        	getNavigator().addProvider(new CustomViewProvider(this, view));
//        	getNavigator().addView(view.getUrlFragment(), view.getViewClass());
		}
        getNavigator().addViewChangeListener(viewChangeListener());
        getNavigator().setErrorView(HomeDesktopView.class);
        setErrorHandler(new UIErrorHandler(this));
    }
	
	
	private ViewChangeListener viewChangeListener() {
		return new ViewChangeListener() {
			public boolean beforeViewChange(ViewChangeEvent event) {
				System.out.println("navigating to: " + event.getNewView().getClass());
				createPresenter((View)event.getNewView());
				return true;
			}
			public void afterViewChange(ViewChangeEvent event) {
			}
		};
	}

	public ErrorMessages getErrorMessages() {
		return errorMessages;
	}

	@Override
	public void beforeClientResponse(boolean initial) {
		super.beforeClientResponse(initial);
		errorMessages.clear();
	}

}