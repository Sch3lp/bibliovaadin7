package be.cegeka;

import be.cegeka.bibliovaadin.component.ErrorMessages;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;

public abstract class View extends CustomComponent implements com.vaadin.navigator.View  {

	private VerticalLayout content;

	public View() {
		content = new VerticalLayout();
		initializeContent(content);
		setCompositionRoot(content);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	protected abstract void initializeContent(Layout content);

	public void setErrorMessages(ErrorMessages errorMessages) {
		content.addComponentAsFirst(errorMessages);
	}

}
