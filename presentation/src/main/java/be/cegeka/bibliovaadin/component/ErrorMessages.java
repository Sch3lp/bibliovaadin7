package be.cegeka.bibliovaadin.component;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class ErrorMessages extends VerticalLayout {

	private static final String ALL_FINE_AND_DANDY = "All fine and dandy";
	
	private Label errorLabel = new Label(ALL_FINE_AND_DANDY);

	public ErrorMessages() {
		addComponent(errorLabel);
	}
	
	public void addMessage(String message) {
		errorLabel.setValue(message);
	}
	
	public void clear() {
		errorLabel.setValue(ALL_FINE_AND_DANDY);
	}
	
}
