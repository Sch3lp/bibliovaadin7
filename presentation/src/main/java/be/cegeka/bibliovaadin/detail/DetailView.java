package be.cegeka.bibliovaadin.detail;

import be.cegeka.View;

import com.cegeka.bibliothouris.BeanLocator;
import com.cegeka.bibliothouris.application.BookService;
import com.cegeka.bibliothouris.domain.Book;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

public class DetailView extends View {

	private Button homeButton;
	private Label titleLabel;
	private TextField bookNameField;
	private TextField bookIsbnField;
	private Book currentBook;

	@Override
	protected void initializeContent(Layout content) {
		homeButton = new Button("home");
		titleLabel = new Label("Book details");
		bookNameField = new TextField("Name");
		bookIsbnField = new TextField("ISBN");
		
		FormLayout fl = new FormLayout();
		fl.setSizeUndefined();
		fl.addComponent(bookIsbnField);
		fl.addComponent(bookNameField);
		
		content.addComponent(titleLabel);
		content.addComponent(fl);
		content.addComponent(homeButton);
	}

	void addHomeButtonListener(ClickListener listener) {
		homeButton.addClickListener(listener);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String parameters = event.getParameters();
		long bookId = Long.parseLong(parameters);
		currentBook = getBookService().get(bookId);
		
//		FieldGroup binder = new FieldGroup(new BeanItem<Book>(currentBook));
		BeanFieldGroup<Book> binder = new BeanFieldGroup<>(Book.class);
		binder.setItemDataSource(currentBook);
		binder.bind(bookNameField, "title");
		binder.bind(bookIsbnField, "isbn.isbnValue");
	}

	private BookService getBookService() {
		return BeanLocator.getBean(BookService.class);
	}

}
