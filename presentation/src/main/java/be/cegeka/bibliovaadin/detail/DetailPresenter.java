package be.cegeka.bibliovaadin.detail;

import static be.cegeka.Screen.HOME;
import be.cegeka.Presenter;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class DetailPresenter extends Presenter<DetailView>  {

	public DetailPresenter(DetailView view) {
		super(view);
		view.addHomeButtonListener(homeButtonListener());
	}

	private ClickListener homeButtonListener() {
		return new ClickListener() {
			public void buttonClick(ClickEvent event) {
				navigateTo(HOME);
			}
		};
	}


}
