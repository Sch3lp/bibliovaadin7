package be.cegeka.bibliovaadin.insert;

import static be.cegeka.Screen.HOME;
import be.cegeka.Presenter;

import com.cegeka.bibliothouris.BeanLocator;
import com.cegeka.bibliothouris.application.BookService;
import com.cegeka.bibliothouris.domain.Book;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class InsertPresenter extends Presenter<InsertView> {

	private BookService bookApplicationService = BeanLocator.getBean(BookService.class);
	
	public InsertPresenter(InsertView view) {
		super(view);
		view.addAddBookButtonListener(addBookListener());
		view.addCancelButtonListener(cancelListener());
	}

	private ClickListener addBookListener() {
		return new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				view.commit();
				Book newBook = view.getNewBook();
				bookApplicationService.add(newBook);
				navigateTo(HOME);
			}
		};
	}

	private ClickListener cancelListener() {
		return new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				navigateTo(HOME);
			}
		};
	}

}
