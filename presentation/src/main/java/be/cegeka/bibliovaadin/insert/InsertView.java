package be.cegeka.bibliovaadin.insert;

import be.cegeka.View;

import com.cegeka.bibliothouris.domain.Book;
import com.cegeka.bibliothouris.domain.Isbn;
import com.cegeka.bibliothouris.domain.Person;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

public class InsertView extends View {

	private Book newBook;
	
	private Button addButton;
	private Button cancelButton;
	private Label titleLabel;
	private TextField bookIsbnField;
	private TextField bookTitleField;
	private TextField authorNameField;
	private TextField authorFirstNameField;
	private BeanFieldGroup<Book> binder;
	
	@Override
	protected void initializeContent(Layout content) {
		addButton = new Button("Add book");
		cancelButton = new Button("Cancel");
		titleLabel = new Label("Add a book");
		bookIsbnField = new TextField("ISBN");
		bookTitleField = new TextField("Title");
		authorNameField = new TextField("Author Name");
		authorFirstNameField = new TextField("Author First Name");
		binder = new BeanFieldGroup<>(Book.class);
		
		FormLayout fl = new FormLayout();
		fl.setSizeUndefined();
		fl.addComponent(bookIsbnField);
		fl.addComponent(bookTitleField);
		fl.addComponent(authorFirstNameField);
		fl.addComponent(authorNameField);
		
		content.addComponent(titleLabel);
		content.addComponent(fl);
		content.addComponent(addButton);
		content.addComponent(cancelButton);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		newBook = new Book();
		newBook.setIsbn(new Isbn("4545454"));
		newBook.setAuthor(new Person("iemand","persoon"));
		
		binder.setItemDataSource(newBook);
		binder.bind(bookIsbnField, "isbn.isbnValue");
		binder.bind(bookTitleField, "title");
		binder.bind(authorFirstNameField, "author.firstname");
		binder.bind(authorNameField, "author.name");
	}
	
	void commit() {
		try {
			binder.commit();
		} catch (CommitException e) {
			throw new RuntimeException(e);
		}
	}
	
	void addAddBookButtonListener(ClickListener listener) {
		addButton.addClickListener(listener);
	}
	
	void addCancelButtonListener(ClickListener listener) {
		cancelButton.addClickListener(listener);
	}
	
	public Book getNewBook() {
		return newBook;
	}

}
