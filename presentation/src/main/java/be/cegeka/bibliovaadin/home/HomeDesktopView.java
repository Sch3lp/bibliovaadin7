package be.cegeka.bibliovaadin.home;

import java.util.List;

import com.cegeka.bibliothouris.BeanLocator;
import com.cegeka.bibliothouris.application.BookService;
import com.cegeka.bibliothouris.domain.Book;
import com.cegeka.bibliothouris.domain.Isbn;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Table;

public class HomeDesktopView extends HomeView {

	private Label titleLabel;
	private Table bookTable;
	private Button addBookButton;
	private Layout buttonsLayout;

	private BookService bookApplicationService;
	
	@Override
	protected void initializeContent(Layout content) {
		bookApplicationService = BeanLocator.getBean(BookService.class);
		titleLabel = new Label("List of books");
		bookTable = createBookTable();
		addBookButton = new Button("Add book");
		buttonsLayout = createButtonsLayout();
		
		content.addComponent(titleLabel);
		content.addComponent(bookTable);
		content.addComponent(addBookButton);
		content.addComponent(buttonsLayout);
		initBookList();
	}

	@Override
	public void addSelectBookListener(ItemClickListener selectBookListener) {
		bookTable.addItemClickListener(selectBookListener);
	}
	
	@Override
	public void addAddBookListener(ClickListener addBookListener) {
		addBookButton.addClickListener(addBookListener);
	}

	private void initBookList() {
		List<Book> books = findAllBooks();
		for (Book book : books) {
			bookTable.addItem(new Object[] {book.getIsbn(), book.getTitle()}, book.getId());
		}
	}

	private List<Book> findAllBooks() {
		return bookApplicationService.getAllBooks();
	}

	private Table createBookTable() {
		Table bookTable = new Table("Books");
		bookTable.addContainerProperty("ISBN", Isbn.class,  null);
		bookTable.addContainerProperty("Title", String.class,  null);
		return bookTable;
	}
	
	private Layout createButtonsLayout() {
		Layout layout = new HorizontalLayout();
		layout.addComponent(new Button("I am a Runo button!"));
		layout.addComponent(createButton("I am a Base button!", "extended-base-button"));
		return layout;
	}

	private Button createButton(String caption, String styleName) {
		Button button = new Button(caption);
		button.setPrimaryStyleName(styleName);
		return button;
	}

}
