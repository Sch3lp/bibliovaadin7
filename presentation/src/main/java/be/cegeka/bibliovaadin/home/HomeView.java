package be.cegeka.bibliovaadin.home;

import be.cegeka.View;

import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button.ClickListener;

public abstract class HomeView extends View {

	public abstract void addSelectBookListener(ItemClickListener selectBookListener);	
	public abstract void addAddBookListener(ClickListener addBookListener);
	
}
