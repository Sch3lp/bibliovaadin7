package be.cegeka.bibliovaadin.home;

import java.util.List;

import com.cegeka.bibliothouris.BeanLocator;
import com.cegeka.bibliothouris.application.BookService;
import com.cegeka.bibliothouris.domain.Book;
import com.cegeka.bibliothouris.domain.Isbn;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Table;

public class HomeMobileView extends HomeView {

	private Label titleLabel;
	private Table bookTable;
	private Button addBookButton;

	private BookService bookApplicationService;
	
	@Override
	protected void initializeContent(Layout content) {
		bookApplicationService = BeanLocator.getBean(BookService.class);
		titleLabel = new Label("List of books");
		bookTable = createBookTable();
		addBookButton = new Button("Add book");
		
		content.addComponent(new Label("MOBILE VERSION!!!"));
		
		content.addComponent(titleLabel);
		content.addComponent(bookTable);
		content.addComponent(addBookButton);
		initBookList();
	}

	@Override
	public void addSelectBookListener(ItemClickListener selectBookListener) {
		bookTable.addItemClickListener(selectBookListener);
	}
	
	@Override
	public void addAddBookListener(ClickListener addBookListener) {
		addBookButton.addClickListener(addBookListener);
	}

	private void initBookList() {
		List<Book> books = findAllBooks();
		for (Book book : books) {
			bookTable.addItem(new Object[] {book.getIsbn(), book.getTitle()}, book.getId());
		}
	}

	private List<Book> findAllBooks() {
		return bookApplicationService.getAllBooks();
	}

	private Table createBookTable() {
		Table bookTable = new Table("Books");
		bookTable.addContainerProperty("ISBN", Isbn.class,  null);
		bookTable.addContainerProperty("Title", String.class,  null);
		return bookTable;
	}

}
