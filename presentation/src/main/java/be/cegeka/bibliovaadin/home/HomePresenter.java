package be.cegeka.bibliovaadin.home;

import be.cegeka.Presenter;
import be.cegeka.Screen;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class HomePresenter extends Presenter<HomeView>  {

	public HomePresenter(HomeView view) {
		super(view);
		view.addSelectBookListener(selectBookListener());
		view.addAddBookListener(addBookListener());
	}

	private ItemClickListener selectBookListener() {
		return new ItemClickListener() {
			
			@Override
			public void itemClick(ItemClickEvent event) {
				Long id = (Long) event.getItemId();
				navigateTo(Screen.DETAIL, Long.toString(id));
			}
		};
	}
	
	private ClickListener addBookListener() {
		return new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				navigateTo(Screen.INSERT);
			}
		};
	}


}
