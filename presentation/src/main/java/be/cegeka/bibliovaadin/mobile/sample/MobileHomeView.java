package be.cegeka.bibliovaadin.mobile.sample;

import java.util.Date;

import be.cegeka.View;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.VerticalLayout;

public class MobileHomeView extends View {

	private Layout content;
	private Label titleLabel;

	private ComboBox comboBox;
	private PopupDateField datePopup;
	
	@Override
	protected void initializeContent(Layout content) {
		content = new VerticalLayout();
		titleLabel = new Label("List of books");
		comboBox = createComboBox();
		datePopup = createDatePopup();
		
		content.addComponent(titleLabel);
		content.addComponent(comboBox);
		content.addComponent(datePopup);
	}

	private PopupDateField createDatePopup() {
		PopupDateField datefield = new PopupDateField("starcraft time", new Date());
		return datefield;
	}

	private ComboBox createComboBox() {
		String[] cities = new String[] { "Berlin", "Brussels", "Helsinki", "Madrid", "Oslo", "Paris", "Stockholm" };

		ComboBox l = new ComboBox("Please select a city");
        for (int i = 0; i < cities.length; i++) {
            l.addItem(cities[i]);
        }

        l.setNullSelectionAllowed(false);
        l.setValue("Berlin");
        l.setImmediate(true);
        l.setReadOnly(true);
        return l;
	}

}
