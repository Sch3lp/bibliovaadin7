package be.cegeka;

import com.vaadin.server.ErrorEvent;
import com.vaadin.server.ErrorHandler;

public class UIErrorHandler implements ErrorHandler {
	
	private BiblioVaadinUI ui;

	public UIErrorHandler(BiblioVaadinUI ui) {
		this.ui = ui;
	}

	@Override
	public void error(ErrorEvent event) {
		Throwable throwable = event.getThrowable();

		ui.getErrorMessages().addMessage(throwable.getMessage());
	}

}
