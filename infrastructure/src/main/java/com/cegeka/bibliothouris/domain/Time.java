package com.cegeka.bibliothouris.domain;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class Time {

    public static Date getTodayTime() {
        Calendar cal = new GregorianCalendar();
        Date date = new Date(convertStringDateToLong("" + cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH) + 1) + "-"
                + cal.get(Calendar.YEAR)));
        return date;
    }

    public static long convertStringDateToLong(String stringDate) {
        long longDate = 0;
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = formatter.parse(stringDate);
            longDate = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return longDate;
    }

}
