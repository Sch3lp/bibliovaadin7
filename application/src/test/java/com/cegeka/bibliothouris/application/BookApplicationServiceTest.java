package com.cegeka.bibliothouris.application;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import com.cegeka.bibliothouris.domain.Book;
import com.cegeka.bibliothouris.domain.BookRepository;
import com.cegeka.bibliothouris.domain.BookTestBuilder;
import com.cegeka.bibliothouris.domain.Isbn;
import com.cegeka.bibliothouris.domain.Person;
import com.cegeka.bibliothouris.test.TestCase;


public class BookApplicationServiceTest extends TestCase {

    private BookApplicationService bookApplicationService;

    private Book book1, book2, book3, book4, book5, book6, book7, book8;

    private List<Book> listWithBook1and2, listWithBook1, listWithBooksWithoutWildcards,
            listWithBooksWithWildcardInBegin, listWithBooksWithWildcardInEnd, listWithBooksWithTwoWildcards,
            listWithBooksWithOnlyWildcard;

    private Isbn isbn1, isbn2, isbn3, isbn4, isbn5;

    private Person person1, person2, person3, person4, person5;

    @Mock
    private BookRepository bookRepositoryMock;

    @Before
    public void setUpSubject() {
        isbn1 = new Isbn("12345");
        isbn2 = new Isbn("54321");
        isbn3 = new Isbn("123");
        isbn4 = new Isbn("345");
        person1 = new Person("Naam 1", "Voornaam 1");
        person2 = new Person("Naam 2", "Voornaam 2");
        person3 = new Person("Naam 3", "Voornaam 3");
        person4 = new Person("Naam 4", "Voornaam 4");
        book5 = new Book(isbn1, "Title 1", person1);
        book6 = new Book(isbn2, "Title 2", person2);
        book7 = new Book(isbn3, "Title 3", person3);
        book8 = new Book(isbn4, "Title 4", person4);
        listWithBooksWithoutWildcards = new ArrayList<Book>();
        listWithBooksWithWildcardInBegin = new ArrayList<Book>();
        listWithBooksWithWildcardInEnd = new ArrayList<Book>();
        listWithBooksWithTwoWildcards = new ArrayList<Book>();
        listWithBooksWithOnlyWildcard = new ArrayList<Book>();
        listWithBooksWithoutWildcards.add(book5);
        listWithBooksWithWildcardInBegin.add(book7);
        listWithBooksWithWildcardInEnd.add(book8);
        listWithBooksWithTwoWildcards.add(book5);
        listWithBooksWithTwoWildcards.add(book6);
        listWithBooksWithTwoWildcards.add(book7);
        listWithBooksWithTwoWildcards.add(book8);
        listWithBooksWithOnlyWildcard.add(book5);
        listWithBooksWithOnlyWildcard.add(book6);
        listWithBooksWithOnlyWildcard.add(book7);
        listWithBooksWithOnlyWildcard.add(book8);
        book1 = new BookTestBuilder().withIsbn(new Isbn("123456")).withTitle("SpringInAction2")
            .withPerson(new Person("Vanoverloop", "Jonathan")).build();
        book2 = new BookTestBuilder().withIsbn(new Isbn("654321")).withTitle("Design Patterns")
            .withPerson(new Person("Wallenus", "Jeroen")).build();
        bookApplicationService = new BookApplicationService(bookRepositoryMock);
        book3 = new BookTestBuilder().build();
        isbn5 = new Isbn("0123");
        person5 = new Person("Name01", "Fname01");
        book4 = new Book(isbn5, "Title01", person5);
        listWithBook1and2 = new ArrayList<Book>();
        listWithBook1and2.add(book1);
        listWithBook1and2.add(book2);
        listWithBook1 = new ArrayList<Book>();
        listWithBook1.add(book1);
    }

    @Test
    public void shouldFindAllTheBooksFromRepository() {
        when(bookRepositoryMock.findAllBooks()).thenReturn(singletonList(book1));

        List<Book> result = bookApplicationService.getAllBooks();

        assertEquals(1, result.size());
        assertTrue(new ReflectionEquals(book1).matches(result.get(0)));
    }

    @Test
    public void shouldStoreNewBookInRepository() {
        bookApplicationService.add(book3);
        verify(bookRepositoryMock).store(book3);
    }

    @Test
    public void shouldFindBookWithIsbn() {
        Isbn isbn = new Isbn("0123");
        when(bookRepositoryMock.findBookBySingleISBN(isbn)).thenReturn(book4);
        bookApplicationService.getBookOnISBN(isbn);
        verify(bookRepositoryMock).findBookBySingleISBN(isbn);
    }

    @Test
    public void searchOnISBNTest() {
        findBooksByIsbnTest(listWithBooksWithoutWildcards, new Isbn("12345"));
        findBooksByIsbnTest(listWithBooksWithWildcardInBegin, new Isbn("*3"));
        findBooksByIsbnTest(listWithBooksWithWildcardInEnd, new Isbn("3*"));
        findBooksByIsbnTest(listWithBooksWithTwoWildcards, new Isbn("*3*"));
        findBooksByIsbnTest(listWithBooksWithOnlyWildcard, new Isbn("*"));
    }

    private void findBooksByIsbnTest(List<Book> listToReturn, Isbn wildcard) {
        when(bookRepositoryMock.findBooksByISBN(wildcard)).thenReturn(listToReturn);
        bookApplicationService.getBooksOnISBN(wildcard);
        verify(bookRepositoryMock).findBooksByISBN(wildcard);
    }

    @Test
    public void shouldFindBookWithName() {
        findBooksByAuthorTest(listWithBook1and2, "J");
        findBooksByAuthorTest(listWithBook1, "th");
    }

    private void findBooksByAuthorTest(List<Book> listToReturn, String author) {
        when(bookRepositoryMock.findBooksByAuthor(author)).thenReturn(listToReturn);
        bookApplicationService.getBooksOnAuthor(author);
        verify(bookRepositoryMock).findBooksByAuthor(author);
    }

    @Test
    public void searchOnTitleTest() {
        List<Book> listTitles = new ArrayList<Book>();
        listTitles.add(book1);
        listTitles.add(book2);

        when(bookRepositoryMock.findBooksOnTitle("Design")).thenReturn(listTitles);
        bookApplicationService.getBooksOnTitle("Design");
        verify(bookRepositoryMock).findBooksOnTitle("Design");
    }
}
