package com.cegeka.bibliothouris.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import com.cegeka.bibliothouris.domain.Adress;
import com.cegeka.bibliothouris.domain.Member;
import com.cegeka.bibliothouris.domain.MemberRepository;
import com.cegeka.bibliothouris.domain.Person;
import com.cegeka.bibliothouris.test.TestCase;


public class MemberApplicationServiceTest extends TestCase {

    private MemberApplicationService memberApplicationService;

    @Mock
    private MemberRepository memberRepositoryMock;

    private Member member1, member2, member3;

    private List<String> listWithInszNumbers;

    private List<Member> listWithAllMembers;

    private Adress adress1, adress2, adress3;

    private Person person1, person2, person3;

    @Before
    public void setUp() {
        adress1 = new Adress("Z", "11", "3210", "E");
        adress2 = new Adress("Street", "23", "3222", "Brussel");
        adress3 = new Adress("Pol", "3", "3", "Zee");
        person1 = new Person("Q", "A");
        person2 = new Person("Name", "FName");
        person3 = new Person("Pol", "Pol");
        memberApplicationService = new MemberApplicationService(memberRepositoryMock);
        member1 = new Member("343", person1, adress1);
        member2 = new Member("434", person2, adress2);
        member3 = new Member("334", person3, adress3);
        listWithInszNumbers = new ArrayList<String>();
        listWithInszNumbers.add("343");
        listWithInszNumbers.add("434");
        listWithInszNumbers.add("334");

        listWithAllMembers = new ArrayList<Member>();
        listWithAllMembers.add(member1);
        listWithAllMembers.add(member2);

    }

    @Test
    public void shouldFindAllMembersFromRepository() {
        when(memberRepositoryMock.findAll()).thenReturn(listWithAllMembers);
        List<Member> result = memberRepositoryMock.findAll();
        assertEquals(2, result.size());
    }

    @Test
    public void shouldStoreNewMemberInRepository() {
        memberApplicationService.add(member3);
        verify(memberRepositoryMock).store(member3);
    }

    @Test
    public void shouldFindAllInszFromRepository() {
        when(memberRepositoryMock.findAllInsz()).thenReturn(listWithInszNumbers);
        List<String> result = memberRepositoryMock.findAllInsz();
        assertEquals(3, result.size());
        assertTrue(new ReflectionEquals(listWithInszNumbers).matches(memberRepositoryMock.findAllInsz()));
    }
}
