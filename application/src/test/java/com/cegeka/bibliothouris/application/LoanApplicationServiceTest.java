package com.cegeka.bibliothouris.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import com.cegeka.bibliothouris.domain.Adress;
import com.cegeka.bibliothouris.domain.Book;
import com.cegeka.bibliothouris.domain.BookTestBuilder;
import com.cegeka.bibliothouris.domain.Isbn;
import com.cegeka.bibliothouris.domain.Loan;
import com.cegeka.bibliothouris.domain.LoanRepository;
import com.cegeka.bibliothouris.domain.Member;
import com.cegeka.bibliothouris.domain.Person;
import com.cegeka.bibliothouris.domain.Time;
import com.cegeka.bibliothouris.test.TestCase;


public class LoanApplicationServiceTest extends TestCase {

    private LoanApplicationService loanApplicationService;

    @Mock
    private LoanRepository loanRepositoryMock;

    private Loan loan1, loan2, loan3, loan4;

    private Book book1, book2;

    private Member member1, member2;

    private Adress adress1, adress2;

    private Person person1, person2;

    private List<Loan> listWithAllLoans, listWithOverdueLoans;

    private List<Isbn> listWithAllIsbnFromMember1, listWithAllIsbnFromMember2;

    @Before
    public void setUp() {
        adress1 = new Adress("holastraat", "15", "6832", "Leuven");
        adress2 = new Adress("Brusselsesteenweg", "57", "3090", "Overijse");
        person1 = new Person("Detoffe", "Thomas");
        person2 = new Person("Deplezante", "Stijn");
        loanApplicationService = new LoanApplicationService(loanRepositoryMock);
        book1 = new BookTestBuilder().withIsbn(new Isbn("333-33-333-3300-0")).withPerson(new Person("Coninx", "Tom"))
            .withTitle("Ik ben een voetbalfan").build();
        book2 = new BookTestBuilder().withIsbn(new Isbn("333-33-333-3330-7"))
            .withPerson(new Person("Grootaerts", "Walter")).withTitle("Ik ben een zanger").build();
        member1 = new Member("839424", person1, adress1);
        member2 = new Member("623874", person2, adress2);
        loan1 = new Loan(book1.getIsbn(), member1.getInsz(), new Date(Time.convertStringDateToLong("02-02-2011")),
            new Date(Time.convertStringDateToLong("23-02-2011")));
        loan2 = new Loan(book2.getIsbn(), member2.getInsz(), new Date(Time.convertStringDateToLong("09-01-2011")),
            new Date(Time.convertStringDateToLong("30-01-2011")));
        loan3 = new Loan(book1.getIsbn(), member2.getInsz(), new Date(Time.convertStringDateToLong("19-03-2011")),
            new Date(Time.convertStringDateToLong("31-12-8000")));
        loan4 = new Loan(book1.getIsbn(), member2.getInsz(), new Date(Time.convertStringDateToLong("10-01-2011")),
            new Date(Time.convertStringDateToLong("17-01-2011")));
        listWithAllLoans = new ArrayList<Loan>();
        listWithOverdueLoans = new ArrayList<Loan>();
        listWithAllLoans.add(loan1);
        listWithAllLoans.add(loan2);
        listWithOverdueLoans.add(loan1);
        listWithOverdueLoans.add(loan2);
        listWithOverdueLoans.add(loan3);
        listWithOverdueLoans.add(loan4);
        listWithAllIsbnFromMember1 = new ArrayList<Isbn>();
        listWithAllIsbnFromMember2 = new ArrayList<Isbn>();
        listWithAllIsbnFromMember1.add(book1.getIsbn());
        listWithAllIsbnFromMember2.add(book2.getIsbn());
        listWithAllIsbnFromMember2.add(book1.getIsbn());
    }

    @Test
    public void shouldFindAllLoansFromRepository() {
        when(loanRepositoryMock.findAll()).thenReturn(listWithAllLoans);
        List<Loan> result = loanApplicationService.getAllLoanedBooks();
        assertEquals(2, result.size());
        assertTrue(new ReflectionEquals(loan1).matches(result.get(0)));
        assertTrue(new ReflectionEquals(loan2).matches(result.get(1)));
    }

    @Test
    public void shouldStoreNewLoanInRepository() {
        loanApplicationService.add(loan1);
        verify(loanRepositoryMock).store(loan1);
        loanApplicationService.add(loan2);
        verify(loanRepositoryMock).store(loan2);
    }

    @Test
    public void shouldReturnAllOverdueBooks() {
        when(loanRepositoryMock.findAll()).thenReturn(listWithOverdueLoans);
        List<Loan> result = loanApplicationService.getOverdueLoans();
        assertEquals(3, result.size());
        assertTrue(new ReflectionEquals(loan1).matches(result.get(0)));
        assertTrue(new ReflectionEquals(loan2).matches(result.get(1)));
        assertTrue(new ReflectionEquals(loan4).matches(result.get(2)));
    }

    @Test
    public void testGetIsbnFromMemberInszTest() {
        getIsbnFromMemberInszTest(listWithAllIsbnFromMember2, member2.getInsz());
        getIsbnFromMemberInszTest(listWithAllIsbnFromMember1, member1.getInsz());
    }

    private void getIsbnFromMemberInszTest(List<Isbn> listToReturn, String memberInsz) {
        when(loanRepositoryMock.findAllIsbnFromMemberInsz(memberInsz)).thenReturn(listToReturn);
        loanApplicationService.findAllIsbnFromMemberInsz(memberInsz);
        verify(loanRepositoryMock).findAllIsbnFromMemberInsz(memberInsz);
    }

}
