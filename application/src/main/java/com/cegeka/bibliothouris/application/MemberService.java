package com.cegeka.bibliothouris.application;

import java.util.List;

import com.cegeka.bibliothouris.domain.Loan;
import com.cegeka.bibliothouris.domain.Member;


public interface MemberService {

    void add(Member member);

    List<Member> getMemberOverview();

    List<String> getMemberINSZOverview();

    Member getMemberOnInsz(String insz);

    Member getMemberFromLoan(Loan loan);

    boolean doesMemberExist(String insz);

}
