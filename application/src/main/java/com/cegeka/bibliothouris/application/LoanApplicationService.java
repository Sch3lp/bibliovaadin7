package com.cegeka.bibliothouris.application;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.cegeka.bibliothouris.domain.Isbn;
import com.cegeka.bibliothouris.domain.Loan;
import com.cegeka.bibliothouris.domain.LoanRepository;
import com.cegeka.bibliothouris.domain.Member;

@Service
public class LoanApplicationService implements LoanService {

    private final LoanRepository repository;

    @Inject
    LoanApplicationService(LoanRepository repository) {
        this.repository = repository;
    }

    @Override
    public void add(Loan loan) {
        repository.store(loan);
    }

    @Override
    public List<Loan> getAllLoanedBooks() {
        return repository.findAll();
    }

    @Override
    public Loan returnLoanOnIsbn(Isbn isbn) {
        return repository.findLoanOnIsbn(isbn);
    }

    @Override
    public void removeLoan(Loan loan) {
        repository.remove(loan);
    }

    @Override
    public List<Loan> getOverdueLoans() {
        List<Loan> result = new ArrayList<Loan>();
        for (Loan l : repository.findAll()) {
            if (l.getEndDate().before(Calendar.getInstance().getTime())) {
                result.add(l);
            }
        }
        return result;
    }

    @Override
    public List<Isbn> findAllIsbnFromMemberInsz(String insz) {
        return repository.findAllIsbnFromMemberInsz(insz);
    }

    @Override
    public List<String> getInszMembers() {
        return repository.getMembersInszFromLoan();
    }

    @Override
    public List<Integer> getAllFinesFromMemberInsz(String insz) {
        return repository.getFinesFromMemberInsz(insz);
    }

    @Override
    public boolean isLend(Isbn isbn) {
        boolean lend = false;
        for (Loan loan : getAllLoanedBooks()) {
            if (loan.getIsbn().equals(isbn)) {
                lend = true;
            }
        }
        return lend;
    }

    @Override
    public Loan getLoanFromIsbn(Isbn isbn) {
        Loan loan = null;
        for (Loan loanInOverview : getAllLoanedBooks()) {
            if (loanInOverview.getIsbn().equals(isbn)) {
                loan = loanInOverview;
            }
        }
        return loan;
    }

    @Override
    public boolean doesLoanExist(String memberInsz) {
        boolean exists = false;
        for (String insz : getInszMembers()) {
            if (insz.equals(memberInsz)) {
                exists = true;
            }
        }
        return exists;
    }

    @Override
    public void calculateFine(Member member) {
        int fine = 0;
        for (int fineFromMember : getAllFinesFromMemberInsz(member.getInsz())) {
            fine += fineFromMember;
        }
        member.setFine(fine);
    }

}
