package com.cegeka.bibliothouris.application;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cegeka.bibliothouris.domain.Book;
import com.cegeka.bibliothouris.domain.BookRepository;
import com.cegeka.bibliothouris.domain.Isbn;

@Service
public class BookApplicationService implements BookService {

	private final BookRepository repository;

	@Override
	public boolean doesBookAlreadyExist(Isbn isbn) {
		return getBookOnISBN(isbn) != null; 
	}

	@Inject
	public BookApplicationService(BookRepository repository) {
		this.repository = repository;
	}

	@Override
	public List<Book> getAllBooks() {
		return repository.findAllBooks();
	}

	@Override
	@Transactional
	public void add(Book book) {
		if (doesBookAlreadyExist(book.getIsbn())) {
			throw new RuntimeException("da boek bestaat al man");
		}
		repository.store(book);
	}

	@Override
	public List<Book> getBooksOnISBN(Isbn isbn) {
		return repository.findBooksByISBN(isbn);
	}

	@Override
	public Book getBookOnISBN(Isbn isbn) {
		return repository.findBookBySingleISBN(isbn);
	}

	@Override
	public List<Book> getBooksOnAuthor(String author) {
		return repository.findBooksByAuthor(author);
	}

	@Override
	public List<Book> getBooksOnTitle(String title) {
		return repository.findBooksOnTitle(title);
	}

	@Override
	public Book get(long id) {
		return repository.find(id);
	}

}
