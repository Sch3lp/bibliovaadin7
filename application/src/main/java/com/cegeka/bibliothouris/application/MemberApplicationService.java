package com.cegeka.bibliothouris.application;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.cegeka.bibliothouris.domain.Loan;
import com.cegeka.bibliothouris.domain.Member;
import com.cegeka.bibliothouris.domain.MemberRepository;

@Service
public class MemberApplicationService implements MemberService {

    private final MemberRepository repository;

    @Inject
    MemberApplicationService(MemberRepository repository) {
        this.repository = repository;
    }

    @Override
    public void add(Member member) {
        repository.store(member);
    }

    @Override
    public List<Member> getMemberOverview() {
        return repository.findAll();
    }

    @Override
    public List<String> getMemberINSZOverview() {
        return repository.findAllInsz();
    }

    @Override
    public Member getMemberOnInsz(String insz) {
        return repository.getMemberOnInsz(insz);
    }

    @Override
    public Member getMemberFromLoan(Loan loan) {
        Member member = null;
        for (Member memberInOverview : getMemberOverview()) {
            if (memberInOverview.getInsz().equals(loan.getMemberInsz())) {
                member = memberInOverview;
            }
        }
        return member;
    }

    @Override
    public boolean doesMemberExist(String insz) {
        boolean memberexists = false;
        for (String memberInsz : getMemberINSZOverview()) {
            if (memberInsz.equals(insz)) {
                memberexists = true;
            }
        }
        return memberexists;
    }


}
