package com.cegeka.bibliothouris.application;

import java.util.List;

import com.cegeka.bibliothouris.domain.Isbn;
import com.cegeka.bibliothouris.domain.Loan;
import com.cegeka.bibliothouris.domain.Member;

public interface LoanService {

    void add(Loan loan);

    List<Loan> getAllLoanedBooks();

    Loan returnLoanOnIsbn(Isbn isbn);

    void removeLoan(Loan loan);

    List<Loan> getOverdueLoans();

    List<Isbn> findAllIsbnFromMemberInsz(String insz);

    List<String> getInszMembers();

    List<Integer> getAllFinesFromMemberInsz(String insz);

    boolean isLend(Isbn isbn);

    Loan getLoanFromIsbn(Isbn isbn);

    boolean doesLoanExist(String memberInsz);

    void calculateFine(Member member);
}
