package com.cegeka.bibliothouris.application;

import java.util.List;

import com.cegeka.bibliothouris.domain.Book;
import com.cegeka.bibliothouris.domain.Isbn;

public interface BookService {

    List<Book> getAllBooks();

    Book get(long id);
    
    void add(Book book);

    List<Book> getBooksOnISBN(Isbn isbn);

    Book getBookOnISBN(Isbn isbn);

    List<Book> getBooksOnAuthor(String author);

    List<Book> getBooksOnTitle(String title);

    boolean doesBookAlreadyExist(Isbn isbn);

}
